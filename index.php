<?php
    require('frog.php');
    require('ape.php');
    $objAnimal1 = new Animal('Shaun');
    echo "Animal Name :".$objAnimal1->name."<br>";
    echo "Legs : ".$objAnimal1->legs."<br>";
    echo "Cold blooded : ".$objAnimal1->cold_blooded."<br> <br>";
    
    $objFrog = new Frog("Katak");
    echo "Animal Name : ".$objFrog->name."<br>";
    echo "Animal Legs : ".$objFrog->legs."<br>";
    echo "Cold blooded : ".$objFrog->cold_blooded."<br>";
    echo "Jump : ".$objFrog->jump()."<br> <br>";

    $objApe = new Ape("Kera");
    echo "Animal Name : ".$objApe->name."<br>";
    echo "Animal Legs : ".$objApe->legs."<br>";
    echo "Cold blooded : ".$objApe->cold_blooded."<br>";
    echo "Yell : ".$objApe->yell()."<br>";
?>